/* eslint-disable */

export const displayMap = locations => {
    mapboxgl.accessToken =
        'pk.eyJ1Ijoicm9vczE3MDUiLCJhIjoiY2s3YWlnZ2I5MTBtODNtczRzOHQ2N3JpaSJ9.ogZoge5cu9Lb_wcnUf9z4w';
    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/roos1705/ck7aik2h00o6s1ikvfnphuyht',
        scrollZoom: false
        // center: [-118.181497, 34.141908],
        // zoom: 6,
        // interactive: false
    });

    const bounds = new mapboxgl.LngLatBounds();

    locations.forEach(location => {
        // create marker
        const el = document.createElement('div');
        el.className = 'marker';

        // add marker
        new mapboxgl.Marker({
            element: el,
            anchor: 'bottom'
        })
            .setLngLat(location.coordinates)
            .addTo(map);

        // Add popup
        new mapboxgl.Popup({
            offset: 30
        })
            .setLngLat(location.coordinates)
            .setHTML(`<p>Day ${location.day}: ${location.description}</p>`)
            .addTo(map);

        // extend map bounds to include current location
        bounds.extend(location.coordinates);
    });

    map.fitBounds(bounds, {
        padding: {
            top: 200,
            bottom: 150,
            left: 100,
            right: 100
        }
    });
};
