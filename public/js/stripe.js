/* eslint-disable*/
import axios from 'axios';
import { showAlert } from './alert';

const stripe = Stripe('pk_test_HS2MlNnGf9pYaqQF4QGZRsPu00SizPStCU');

export const bookTour = async tourId => {
    try {
        // Get session from api
        const session = await axios(
            `/api/v1/booking/checkout-session/${tourId}`
        );

        // create checkout form + charge credit card
        await stripe.redirectToCheckout({
            sessionId: session.data.session.id
        });
    } catch (err) {
        console.log(err);
        showAlert('error', err);
    }
};
